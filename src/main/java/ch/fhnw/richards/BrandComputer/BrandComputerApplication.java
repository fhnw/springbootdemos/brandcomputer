package ch.fhnw.richards.BrandComputer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BrandComputerApplication {

	public static void main(String[] args) {
		SpringApplication.run(BrandComputerApplication.class, args);
	}

}
