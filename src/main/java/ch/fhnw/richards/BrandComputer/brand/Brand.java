package ch.fhnw.richards.BrandComputer.brand;

import ch.fhnw.richards.BrandComputer.computer.Computer;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import jakarta.persistence.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "brand")
public class Brand {
    @Id
    @GeneratedValue
    @Column(name = "ID")
    private Integer ID;
    @Column(name = "brandName")
    private String brandName;
    @Column(name = "country")
    private String country;
    @OneToMany(mappedBy = "brand", fetch = FetchType.EAGER)
    @JsonManagedReference
    private List<Computer> computers = new ArrayList<>();

    public Brand() {}

    public Brand(Integer ID, String brandName, String country) {
        this.ID = ID;
        this.brandName = brandName;
        this.country = country;
    }

    public Integer getID() {
        return ID;
    }

    public void setID(Integer ID) {
        this.ID = ID;
    }

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public List<Computer> getComputers() {
        return computers;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Brand brand = (Brand) o;
        return Objects.equals(ID, brand.ID);
    }

    @Override
    public int hashCode() {
        return Objects.hash(ID);
    }
}
