package ch.fhnw.richards.BrandComputer.brand;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class BrandController {
    private final BrandRepository repository;

    BrandController(BrandRepository repository) {
        this.repository = repository;
    }

    @GetMapping("/brands")
    List<Brand> all() {
        return repository.findAll();
    }

    @GetMapping("/brands/{id}")
    Brand one(@PathVariable Integer id) {
        return repository.findById(id)
                .orElseThrow(() -> new BrandNotFoundException(id));
    }
}
