package ch.fhnw.richards.BrandComputer.brand;

public class BrandNotFoundException extends RuntimeException {
    BrandNotFoundException(Integer id) {
        super("Could not find brand " + id);
    }
}
