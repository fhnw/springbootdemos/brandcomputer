package ch.fhnw.richards.BrandComputer;

import ch.fhnw.richards.BrandComputer.brand.Brand;
import ch.fhnw.richards.BrandComputer.brand.BrandRepository;
import ch.fhnw.richards.BrandComputer.computer.Computer;
import ch.fhnw.richards.BrandComputer.computer.ComputerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class LoadDatabase implements CommandLineRunner {
    @Autowired
    private BrandRepository brandRepository;
    @Autowired
    private ComputerRepository computerRepository;
    
    @Override
    public void run(String... args) throws Exception {
        Brand b1 = new Brand(1, "Dell", "USA");
        brandRepository.save(b1);
        Brand b2 = new Brand(2, "Lenovo", "China");
        brandRepository.save(b2);

        Computer c11 = new Computer("XPS", "Kreative und Kreativität", 850);
        c11.setBrand(b1);
        computerRepository.save(c11);
        Computer c12 = new Computer("Latitude", "Performance und Zusammenarbeit im Unternehmen", 800);
        c12.setBrand(b1);
        computerRepository.save(c12);
        Computer c21 = new Computer("ThinkPad", "Flexibility to the max", 1220);
        c21.setBrand(b2);
        computerRepository.save(c21);
        Computer c22 = new Computer("ThinkBook", "Every day, elevated experiences", 760);
        c22.setBrand(b2);
        computerRepository.save(c22);
    }
}
