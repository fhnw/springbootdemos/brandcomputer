package ch.fhnw.richards.BrandComputer.computer;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class ComputerController {
    private final ComputerRepository repository;

    ComputerController(ComputerRepository repository) {
        this.repository = repository;
    }

    @GetMapping("/computers")
    List<Computer> all() {
        return repository.findAll();
    }

    @GetMapping("/computers/{model}")
    Computer one(@PathVariable String model) {
        return repository.findById(model)
                .orElseThrow(() -> new ComputerNotFoundException(model));
    }
}
