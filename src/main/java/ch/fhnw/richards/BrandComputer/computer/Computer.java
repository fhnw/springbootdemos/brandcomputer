package ch.fhnw.richards.BrandComputer.computer;

import ch.fhnw.richards.BrandComputer.brand.Brand;
import com.fasterxml.jackson.annotation.JsonBackReference;
import jakarta.persistence.*;

import java.util.Objects;

@Entity
@Table(name = "computer")
public class Computer {
    @Id
    @Column(name = "model")
    private String model;
    @Column(name = "description")
    private String description;
    @Column(name = "price")
    private Integer price;
    @ManyToOne
    @JoinColumn(name = "brandID")
    @JsonBackReference
    private Brand brand;

    public Computer() {}

    public Computer(String model, String description, Integer price) {
        this.model = model;
        this.description = description;
        this.price = price;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public Brand getBrand() {
        return brand;
    }

    public void setBrand(Brand brand) {
        this.brand = brand;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Computer computer = (Computer) o;
        return Objects.equals(model, computer.model);
    }

    @Override
    public int hashCode() {
        return Objects.hash(model);
    }
}
