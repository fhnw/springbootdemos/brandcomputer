package ch.fhnw.richards.BrandComputer.computer;

import org.springframework.data.jpa.repository.JpaRepository;

public interface ComputerRepository extends JpaRepository<Computer, String> {
}
