package ch.fhnw.richards.BrandComputer.computer;

public class ComputerNotFoundException extends RuntimeException {
    ComputerNotFoundException(String model) {
        super("Could not find computer " + model);
    }
}
